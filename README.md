Travis Dattilo  
travisdattilo@yahoo.com  

"prog2_1.hpp"  
This header file contains the private and public methods used in "prog2_1.cpp." The public methods consist of the typical 
constructor and destructor, as well as a Tokenize and GetTokens (these two methods and their functionality are described 
below). The private methods consist of validStrings, validInteger, clearTokenStorage, and clearOutputVector which are all 
helper methods to Tokenize and GetTokens that help make my program clear and understandable.

"prog2_1.cpp"  
This class file uses the "prog2_1.hpp" header file and has the implementations for the methods described in there. There
are four public methods. The constructor, which creates needed data structures like a queue and vectors. The destructor,
which deletes the data structures when the program finishes executing. The void Tokenize method, which takes a single
string argument and tokenizes that string on the space character. This method also checks if the tokens are one of the
following valid tokens: push, pop, add, sub, mul, div, mod, skip, save, get, and any valid integer. Anything contradictory
to these tokens are thrown as an exception as an unexpected token. Finally, the vector of strings function GetTokens,
which will take a single set of entered tokens from Tokenize in a queue fashion. When there are no more sets of tokens to
take, GetTokens will throw a helpful exception message that there are no tokens.

"prog2_2.cpp"  
This driver program utilizes the previous files, "prog2_1.cpp" and "prog2_1.hpp" and takes a single command line argument
that is a file. This file will be tokenized line by line using the Tokenize function from before, and will use GetTokens to
actually retrieve the valid tokens from the file reporting any invalid ones in an exception message. This message will
notify the user what line their erroneous token is on. With no exceptions or errors in the tokenization process, the
tokens will be printed as a comma separated list for each line from the file.

"prog2_3.hpp"  
This header file contains the private and public methods used in "prog2_3.cpp." The public methods consist of the typical
constructor and destructor, as well as a Parse (this method and its functionality is described below). The private methods
consist of isValidDouble, isValidSingle, isValidInteger which are all helper methods to Parse that hepl make my program
clear and understandble.

"prog2_3.cpp"  
This class file uses the "prog2_3.cpp" header file and has the implementations for the methods described in there. There
are three public methods. The constructor, which creates necessary data structures. The destructor, which deletes data
structures after the program is finished executing. The boolean Parse method, which takes a single vector of strings
argument and validates that the input adheres to the following rules: pop, add, sub, mul, div, mod, skip must appear alone
on their own line while push, save, and get must appear with a second integer token, each on their own line as well. Any
input that does not follow these two rules is invalid.

"prog2_4.cpp"  
This driver program utilizes the previous files, "prog2_3.cpp," "prog2_3.hpp," and "prog2_1.hpp," and takes a single 
command line argument that is a file. This file will be tokenized line by line using the Tokenize function from before,
and will use GetTokens to actually retreive the valid tokens from the file reporting invalid ones in an exception message
which is described above. Each line of the file will be parsed using the Parse function which will return true or false
depending on whether or not the tokens are valid and follow the previously described rules. If not, then an exception is
thrown along with a helpful message to identify what line the parse error occured. The error messages from before will
occur as well if they are caused from tokenization errors or a lack of tokens. 

"cs320programmingrubric.pdf"  
This rubric is required for grading purposes.


