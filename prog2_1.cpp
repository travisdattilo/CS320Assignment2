#include "prog2_1.hpp"
#include <queue>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string> 
#include <string.h>  
#include <exception>
#include <vector>
#include <algorithm>
#include <exception>
#include <stdexcept>

using namespace std;

token::token(){       
    this->tokenStorage = new std::queue<string>(); 
    this->getOutputVector =  new std::vector<string>();    
    this->validInput = new std::vector<string>(); 
}

token::~token(){                   
    delete this->tokenStorage;      
    delete this->validInput;
    delete this->getOutputVector; 
}

bool token::validInteger(string userInputString){
    int decimalLocation = userInputString.find(".");  
    return((isdigit(userInputString[0])) && !(decimalLocation >= 0));       
}

bool token::validStrings(string userInputString){
    string validInput[] = {"push","pop","add","sub","mul","div","mod","skip","save","get"};   
    string *beginofList = validInput;
    string *endofList = validInput + 10; 
    return (((find(beginofList, endofList, userInputString)) != endofList) || validInteger(userInputString));       
}

void token::clearTokenStorage(){
    while(!(this->tokenStorage->empty())){       // to clear for the next use of Tokenize;
        this->tokenStorage->pop(); 
    }    
}
void token::clearOutputVector(){
    while(!(this->getOutputVector->empty())){       // to clear for the next use of Tokenize;
        this->getOutputVector->pop_back(); 
    }    
}

void token::Tokenize(string input){
    clearTokenStorage();       
        char charInput[input.size()+1];
        strcpy(charInput, input.c_str());
        char *ptr;
        ptr = strtok(charInput," ");
        while(ptr != NULL){
            string currentToken;
            currentToken.append(ptr); 
            if(validStrings(currentToken)){
                this->tokenStorage->push(currentToken); 
                ptr = strtok(NULL, " ");
            }
            else{
                throw runtime_error("Unexpected token: " + currentToken); 
            } 
        }
}


vector<string> token::GetTokens(){
        clearOutputVector();   
        if(this->tokenStorage->empty()){
            throw invalid_argument("No tokens");
        }
        else{
            while(!(this->tokenStorage->empty())){
                string tmp;
                tmp = this->tokenStorage->front();   
                this->tokenStorage->pop();              
                this->getOutputVector->push_back(tmp);        
            }  

            return *getOutputVector; 
        }
}

