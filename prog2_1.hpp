#include <queue>
#include <queue>
#include <stdio.h>
#include <string> 
#include <string.h>  
#include <exception>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std; 

class token{
    private:
        // Data structure to hold all of elements
        std::queue<string> *tokenStorage;   // Template Generic, queue of strings for GetTokens 
        std::vector<string> *validInput;
        std::vector<string> *getOutputVector;
        bool validStrings(string userInputString);
        bool validInteger(string userInputString);        
        void clearTokenStorage();
        void clearOutputVector();
    public: 
        token();        // Constructor
        ~token();       // Deconstructor
        void Tokenize(string str);    
        vector<string> GetTokens();
};
