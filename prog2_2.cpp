#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <string>
#include <exception>
#include "prog2_1.hpp"
using namespace std;

int main(int argc, char* argv[]){
    token tok; 
    cout << "Assignment #2-2, Travis Dattilo, travisdattilo@yahoo.com" << endl;
    ifstream file(argv[1]);                     // pass file name as argument
    string line; 
    
    int count = 0;
    try{     
        vector<string> toPrint;
        vector<string> toHold;   
        vector<int> howMany;   
        while(!file.eof()){                     // looping through the file  
            string line;        
            getline(file, line);        
            if(line == "")continue;
            tok.Tokenize(line);  
            toHold = tok.GetTokens();
            howMany.push_back(toHold.size());  
            for(int i = 0; i < toHold.size(); i++){
                if(i < howMany[count] - 1){    
                    toPrint.push_back(toHold[i] + ",");
                }      
                else{
                    toPrint.push_back(toHold[i] + "\n");
                }
            }
            count++; 
        }
        for(int i = 0; i < toPrint.size(); i++){    // print after while loop executes in case an exception needs to be thrown
            cout << toPrint[i];
        }
    }      
    catch(exception& e){
        cerr << "Error on line " << count + 1 << ": " << e.what() << endl;   
    } 
    return 0;
}

