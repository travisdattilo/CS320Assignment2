#include "prog2_3.hpp"
#include <queue>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string> 
#include <string.h>  
#include <exception>
#include <vector>
#include <algorithm>
#include <exception>
#include <stdexcept>

using namespace std;


Parser::Parser(){        
    this->input =  new std::vector<string>(); 
}

Parser::~Parser(){                   
    delete this->input;     
}


bool Parser::isValidSingle(string firstToken){  // Checks that the token is alone              
    string validSingle[] = {"pop","add","sub","mul","div","mod","skip"}; 
    string *beginofList = validSingle;
    string *endofList = validSingle + 7; 
    return (((find(beginofList, endofList, firstToken)) != endofList) || isValidInteger(firstToken));
}

bool Parser::isValidDouble(string firstToken){  // Checks that the token is with a valid integer number
    string validDouble[] = {"push","save","get"}; 
    string *beginofList = validDouble;
    string *endofList = validDouble + 3; 
    return (((find(beginofList, endofList, firstToken)) != endofList) || isValidInteger(firstToken));
}

bool Parser::isValidInteger(string secondToken){ 
    return (isdigit(secondToken[0]));   
}

bool Parser::Parse(vector<string> toParse){  
    if(toParse.size() == 2){
        bool check = isValidDouble(toParse[0]) && isValidInteger(toParse[1]);
        return check;        
    }       
    else if(toParse.size() == 1){
        bool check = isValidSingle(toParse[0]);
        return check;    
    }
    else{
        string badString;
        for(int i = 0; i < toParse.size(); i++){
            badString = badString + " " + toParse[i];
        }
        throw invalid_argument(badString);
    }
}



