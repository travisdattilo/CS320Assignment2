#include <queue>
#include <queue>
#include <stdio.h>
#include <string> 
#include <string.h>  
#include <exception>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std; 

class Parser{
    private:
        std::vector<string> *input;
        bool isValidDouble(string firstToken); 
        bool isValidSingle(string firstToken);
        bool isValidInteger(string secondToken);
 
    public: 
        Parser();        // Constructor
        ~Parser();       // Deconstructor
        bool Parse(vector<string> toParse); 
};
