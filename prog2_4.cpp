#include "prog2_1.hpp"
#include "prog2_3.hpp"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <string>
#include <exception>
#include <stdexcept>
using namespace std;

int main(int argc, char* argv[]){
    token tok; 
    Parser pars;
    cout << "Assignment #2-4, Travis Dattilo, travisdattilo@yahoo.com" << endl;
    ifstream file(argv[1]);                     //pass file name as argument
    string line; 

    int count = 0; 
    try{    
        vector<string> toPrint;
        vector<string> toHold; 
        vector<int> howMany;
        string tmp;
        while(!file.eof()){                     // Loops through the file and tokenizes and parses
            string line;        
            getline(file, line);        
            if(line == "")continue;
            tok.Tokenize(line);  
            toHold = tok.GetTokens();       
            howMany.push_back(toHold.size());                             
            bool valid = pars.Parse(toHold);                        
            for(int i = 0; i < toHold.size(); i++){            
                if(valid){   
                    if(i < howMany[count] - 1){    
                        toPrint.push_back(toHold[i] + ",");
                    }      
                    else{
                        toPrint.push_back(toHold[i] + "\n");
                    }   
                }
                else{
                    throw invalid_argument(line);
                }
            }  
            count++;                              
        }  
        for(int i = 0; i < toPrint.size(); i++){    // Prints after loop so error messages can be thrown if they occur
            cout << toPrint[i];
        }                          
    }
    catch(runtime_error& e){
        cerr << "Error on line " << count + 1 << ": " << e.what() << endl;   
    }
    catch(invalid_argument &e){
        cerr << "Parse error on line " << count + 1 << ": " << e.what() << endl;
    }   
    
    return 0;
}

